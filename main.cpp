#include "utility.h"
#include "car.h"
#include <iostream>
#include <cstring>
#include <memory>

void print_help_message()
{
	std::cout << "usage: Astar -i inputfile [-A]" << std::endl;
}

//Astar -i inputfile [-A]
int main(int argc, char* agrv[])
{
	std::unique_ptr<car> testcar_indirect(nullptr);

	switch (argc)
	{
	case 3:
	{
		if (strcmp(agrv[1], "-i"))
		{
			print_help_message();
			return 0;
		}
		const char* mapfile = agrv[2];

		std::cout << "ready? go!\nusing A star planning." << std::endl;

		testcar_indirect = std::unique_ptr<car>(new car_Astar(mapfile));
		break;
	}
	case 4:
	{
		if (strcmp(agrv[1], "-i") || strcmp(agrv[3], "-A"))
		{
			print_help_message();
			return 0;
		}
		const char* mapfile = agrv[2];

		std::cout << "ready? go!\nusing adaptive A star planning." << std::endl;

		testcar_indirect = std::unique_ptr<car>(new car_Adaptive_Astar(mapfile));
		break;
	}
	default:
	{
		std::cout << "too much or too less parameters.\n";
		print_help_message();
		return 0;
	}
	}
	testcar_indirect->going_N_showpath();
	testcar_indirect->print_search_info();
	return 0;
}

