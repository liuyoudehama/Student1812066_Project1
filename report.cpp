#include "utility.h"
#include "car.h"
#include <iostream>
#include <cstring>
#include <fstream>
#include <vector>
#include <string>

int main(int argc, char* argv[])
{
	std::fstream fout;
	fout.open("./report_information", std::ios_base::out);
	std::vector<std::string> inputfiles;
	for (int i = 1; i <= 18; ++i)
	{
		inputfiles.emplace_back("./inputs/" + std::to_string(i) + ".txt");
	}
	if (fout.is_open())
	{
		for (auto& inputfile : inputfiles)
		{
			car* Astar_car = new car_Astar(inputfile);
			car* Adaptive_Astar_car = new car_Adaptive_Astar(inputfile);
			auto info_1 = Astar_car->going_N_showpath();
			auto info_2 = Adaptive_Astar_car->going_N_showpath();
			fout << "* inputfile: " << inputfile << ": \n";
			fout << "   1. using Astar: \n";
			fout << "       - planning times: " << info_1.planning_cnt << "\n";
			fout << "       - total expand nodes:" << info_1.total_expand_cnt << "\n\n";
			fout << "   2. using Adaptive_Astar: \n";
			fout << "       - planning times: " << info_2.planning_cnt << "\n";
			fout << "       - total expand nodes:" << info_2.total_expand_cnt << "\n\n";
		}
	}
	fout << std::endl;
	fout.close();
	return 0;
}