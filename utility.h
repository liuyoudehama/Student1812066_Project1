/*
	&&&&&&&&&&&&&&&&&&&
	&& IN HIGH LEVEL &&
	&&&&&&&&&&&&&&&&&&&

	1.provide some utility to debug
	2.provide some utility to deal with input and output.

*/

#ifndef UTILITY_
#define UTILITY_

#include <vector>
#include <iostream>
#include <utility>
#include <unordered_set>

//#define DEBUG
#define READ_BUFSIZE 1024


//constexpr int read_bufsize = 1024;

namespace functor_util
{
class myhash_forpair
{
public:
	size_t operator()(const std::pair<int, int>& p)const noexcept
	{
		auto h1 = std::hash<int>()(p.first);
		auto h2 = std::hash<int>()(p.second);
		return h1 << 1 ^ h2;
	}
};

};

namespace debug_util
{
template<typename T>
void print_matrix(const std::vector<std::vector<T>>& matrix, std::unordered_set<std::pair<int, int>, functor_util::myhash_forpair>* exception_coor_list_ptr = nullptr, const char exception_notation = 'F')
{
	if (matrix.size() == 0 || matrix[0].size() == 0) return;

	std::cout << "  ";

	for (int col_index = 0; col_index < matrix[0].size(); ++col_index)
	{
		std::cout << col_index;
		if (col_index < 10) std::cout << " ";
	}

	std::cout << "\n";

	for (int row_index = 0; row_index < matrix.size(); ++row_index)
	{
		std::cout << row_index;
		if (row_index < 10) std::cout << " ";

		for (int col_index = 0; col_index < matrix[0].size(); ++col_index)
		{
			if (!exception_coor_list_ptr || exception_coor_list_ptr->find(std::make_pair(row_index, col_index)) == exception_coor_list_ptr->end())
			{
				std::cout << matrix[row_index][col_index] << " ";
			}
			else
			{
				std::cout << exception_notation << " ";
			}
		}
		std::cout << "\n";
	}
	std::cout << std::flush;
}
};

namespace file_util
{
//read the file in the vector<vector<char>> matrix
//read the start point and the goal point as well.
std::vector<std::vector<char>> read_map(const std::string& filename, int& start_x, int& start_y, int& goal_x, int& goal_y);

struct search_info
{
	int planning_cnt;
	int total_expand_cnt;
	search_info(int plan_cnt, int expand_cnt): planning_cnt(plan_cnt), total_expand_cnt(expand_cnt) {}
};
};

/*
namespace container_util
{
class my_openlist
{

public:

};
class my_openlist_method1: public my_openlist
{
public:

}
class my_openlist_method2: public my_openlist
{
public:

};

class my_openlist_method3: public my_openlist
{
public:

};

};
*/
#endif
