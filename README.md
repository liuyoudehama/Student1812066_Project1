
# README(REPORT)
# introduction:
    * This is the self driving car course project1.(requirements is in project1.pdf)
    * iuputs is a map file.
    * basic logic is the "sense-plan-act" cycle.
    * use Astar and adaptive Astar for motion planning.
    * we compared the node expanded in each algorithms.
###### editor: Yihe Wang 

## usage of the srouce code:

**commands to make executable:**

* clone the repo
* make / make build
* use Astar -i "inputfile" [-A] to run.
* make report
* use report to get the output of the experiment.

## answer questions:

### Question 1

Report the following information for each of the 18 provided input files:
#### for f(s1) = f(s2) = mins∈OPEN{f(s) regard the node with smaller gvalue as a priorone:
* inputfile: ./inputs/1.txt: 
   1. using Astar: 
       - planning times: 1
       - total expand nodes:3

   2. using Adaptive_Astar: 
       - planning times: 1
       - total expand nodes:3

* inputfile: ./inputs/2.txt: 
   1. using Astar: 
       - planning times: 2
       - total expand nodes:13

   2. using Adaptive_Astar: 
       - planning times: 2
       - total expand nodes:13

* inputfile: ./inputs/3.txt: 
   1. using Astar: 
       - planning times: 1
       - total expand nodes:20

   2. using Adaptive_Astar: 
       - planning times: 1
       - total expand nodes:20

* inputfile: ./inputs/4.txt: 
   1. using Astar: 
       - planning times: 4
       - total expand nodes:28

   2. using Adaptive_Astar: 
       - planning times: 4
       - total expand nodes:28

* inputfile: ./inputs/5.txt: 
   1. using Astar: 
       - planning times: 4
       - total expand nodes:33

   2. using Adaptive_Astar: 
       - planning times: 4
       - total expand nodes:32

* inputfile: ./inputs/6.txt: 
   1. using Astar: 
       - planning times: 4
       - total expand nodes:77

   2. using Adaptive_Astar: 
       - planning times: 8
       - total expand nodes:211

* inputfile: ./inputs/7.txt: 
   1. using Astar: 
       - planning times: 1
       - total expand nodes:57

   2. using Adaptive_Astar: 
       - planning times: 1
       - total expand nodes:57

* inputfile: ./inputs/8.txt: 
   1. using Astar: 
       - planning times: 26
       - total expand nodes:1292

   2. using Adaptive_Astar: 
       - planning times: 26
       - total expand nodes:1121

* inputfile: ./inputs/9.txt: 
   1. using Astar: 
       - planning times: 5
       - total expand nodes:272

   2. using Adaptive_Astar: 
       - planning times: 5
       - total expand nodes:272

* inputfile: ./inputs/10.txt: 
   1. using Astar: 
       - planning times: 637
       - total expand nodes:431837

   2. using Adaptive_Astar: 
       - planning times: 637
       - total expand nodes:431837

* inputfile: ./inputs/11.txt: 
   1. using Astar: 
       - planning times: 9
       - total expand nodes:551

   2. using Adaptive_Astar: 
       - planning times: 9
       - total expand nodes:551

* inputfile: ./inputs/12.txt: 
   1. using Astar: 
       - planning times: 30
       - total expand nodes:1601

   2. using Adaptive_Astar: 
       - planning times: 30
       - total expand nodes:1543

* inputfile: ./inputs/13.txt: 
   1. using Astar: 
       - planning times: 1
       - total expand nodes:48

   2. using Adaptive_Astar: 
       - planning times: 1
       - total expand nodes:48

* inputfile: ./inputs/14.txt: 
   1. using Astar: 
       - planning times: 57
       - total expand nodes:4392

   2. using Adaptive_Astar: 
       - planning times: 57
       - total expand nodes:4368

* inputfile: ./inputs/15.txt: 
   1. using Astar: 
       - planning times: 22
       - total expand nodes:1068

   2. using Adaptive_Astar: 
       - planning times: 22
       - total expand nodes:1013

* inputfile: ./inputs/16.txt: 
   1. using Astar: 
       - planning times: 40
       - total expand nodes:4983

   2. using Adaptive_Astar: 
       - planning times: 40
       - total expand nodes:4980

* inputfile: ./inputs/17.txt: 
   1. using Astar: 
       - planning times: 4
       - total expand nodes:77

   2. using Adaptive_Astar: 
       - planning times: 8
       - total expand nodes:211

* inputfile: ./inputs/18.txt: 
   1. using Astar: 
       - planning times: 1
       - total expand nodes:12

   2. using Adaptive_Astar: 
       - planning times: 1
       - total expand nodes:12

#### for f(s1) = f(s2) = mins∈OPEN{f(s) regard the node with lagrer gvalue as a priorone:
* inputfile: ./inputs/1.txt: 
   1. using Astar: 
       - planning times: 1
       - total expand nodes:3

   2. using Adaptive_Astar: 
       - planning times: 1
       - total expand nodes:3

* inputfile: ./inputs/2.txt: 
   1. using Astar: 
       - planning times: 2
       - total expand nodes:10

   2. using Adaptive_Astar: 
       - planning times: 2
       - total expand nodes:10

* inputfile: ./inputs/3.txt: 
   1. using Astar: 
       - planning times: 1
       - total expand nodes:8

   2. using Adaptive_Astar: 
       - planning times: 1
       - total expand nodes:8

* inputfile: ./inputs/4.txt: 
   1. using Astar: 
       - planning times: 4
       - total expand nodes:25

   2. using Adaptive_Astar: 
       - planning times: 4
       - total expand nodes:25

* inputfile: ./inputs/5.txt: 
   1. using Astar: 
       - planning times: 4
       - total expand nodes:27

   2. using Adaptive_Astar: 
       - planning times: 4
       - total expand nodes:26

* inputfile: ./inputs/6.txt: 
   1. using Astar: 
       - planning times: 8
       - total expand nodes:163

   2. using Adaptive_Astar: 
       - planning times: 9
       - total expand nodes:185

* inputfile: ./inputs/7.txt: 
   1. using Astar: 
       - planning times: 1
       - total expand nodes:15

   2. using Adaptive_Astar: 
       - planning times: 1
       - total expand nodes:15

* inputfile: ./inputs/8.txt: 
   1. using Astar: 
       - planning times: 23
       - total expand nodes:753

   2. using Adaptive_Astar: 
       - planning times: 26
       - total expand nodes:660

* inputfile: ./inputs/9.txt: 
   1. using Astar: 
       - planning times: 5
       - total expand nodes:74

   2. using Adaptive_Astar: 
       - planning times: 5
       - total expand nodes:74

* inputfile: ./inputs/10.txt: 
   1. using Astar: 
       - planning times: 637
       - total expand nodes:49637

   2. using Adaptive_Astar: 
       - planning times: 637
       - total expand nodes:49637

* inputfile: ./inputs/11.txt: 
   1. using Astar: 
       - planning times: 9
       - total expand nodes:159

   2. using Adaptive_Astar: 
       - planning times: 9
       - total expand nodes:159

* inputfile: ./inputs/12.txt: 
   1. using Astar: 
       - planning times: 30
       - total expand nodes:737

   2. using Adaptive_Astar: 
       - planning times: 30
       - total expand nodes:669

* inputfile: ./inputs/13.txt: 
   1. using Astar: 
       - planning times: 1
       - total expand nodes:13

   2. using Adaptive_Astar: 
       - planning times: 1
       - total expand nodes:13

* inputfile: ./inputs/14.txt: 
   1. using Astar: 
       - planning times: 57
       - total expand nodes:1257

   2. using Adaptive_Astar: 
       - planning times: 57
       - total expand nodes:1100

* inputfile: ./inputs/15.txt: 
   1. using Astar: 
       - planning times: 22
       - total expand nodes:565

   2. using Adaptive_Astar: 
       - planning times: 22
       - total expand nodes:500

* inputfile: ./inputs/16.txt: 
   1. using Astar: 
       - planning times: 40
       - total expand nodes:968

   2. using Adaptive_Astar: 
       - planning times: 40
       - total expand nodes:893

* inputfile: ./inputs/17.txt: 
   1. using Astar: 
       - planning times: 8
       - total expand nodes:163

   2. using Adaptive_Astar: 
       - planning times: 9
       - total expand nodes:185

* inputfile: ./inputs/18.txt: 
   1. using Astar: 
       - planning times: 1
       - total expand nodes:10

   2. using Adaptive_Astar: 
       - planning times: 1
       - total expand nodes:10

### Question 2
* What data structure did you choose to use for your priority queue. Explain your reasoning.
	* operations we did for the openlist is
		1. pick up a smallest one.
		2. delete the smallest one.
		3. check the element is in the node or not.(find)
		4. delete and insert a certain node.(the semantic is "update")
		5. insert a new node.

	* if we choose std::set for the openlist, the time complexity of each operation:
		1. pick up a smallest one.   - O(log n)
		2. delete the smallest one.  - O(log n)
		3. check the element is in the node or not. - O(log n)
		4. delete and insert a certain node() - O(log n)
		5. insert a new node. - O(log n)

### Question 3
* Determine experimentally which tie-breaking criteria (in favor of smaller g- values or larger g-values) results in a smaller number of cell expansions.
* Explain your observation in detail, that is, explain what you observed and give a reason for the observation.
Breaking ties in favor of smaller g-values means that if two cells s1 and s2 in the OPEN list both have the same f-value that is the smallest in the priority queue (i.e., f(s1) = f(s2) = mins∈OPEN{f(s)}) and s1 has a smaller g-value than s2 (i.e., g(s1) < g(s2)), then your algorithm will choose s1 over s2 to pop from the priority queue.

    * my observation:\
      in most of time, larger gvalue will lead to small amount of node expansions.\
      if two nodes have same f-value, expand the larger gvalue one will let the deepest path "deep in".
      however, if we expand the smaller gvalue one will let each candidate path grow equaly.
      if there are many many candidate path, we will expand many many nodes.

### Question 4
Determine experimentally whether Adaptive A* expands fewer cells than A* and try to determine in which situations the savings are large versus small (or non-existing). Explain your reasoning for your observations.
* My opinion:
	- AA* will expand less nodes compared to the simple A* if
	    1. they find the same shortest path every time.\
	 	     which lead the same replanning time. 
      2. if we define "wrong direction" as some not well explored paths that has part of node expanded in last search.
      3. AA* expand less nodes by eliminate to expand some the nodes in "wrong direction" this time.

