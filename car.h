/*
	&&&&&&&&&&&&&&&&&&&
	&& IN HIGH LEVEL &&
	&&&&&&&&&&&&&&&&&&&

	0.a car object is a car entity in a certain real map.
	1.a car is "in a real map".
	2.a car has its view of "real map"
	3-1.car's status include:
		(1) its current position
		(2) its goal
		(3-1) the view of the "real map"

		(3-2) the route planned to go to the goal.

	3-2.car's attributes include:
		(1) the method its used to planning the route.
			which is in the carview map.

	4.car's include:
		(1) detect its surrounding.
		(2) update its view of the "real map"
		(3) planning for the route
		(4) move along the route until we can't move along that route.

	5.how to construct a car?
		(1) provide the map.

*/

/*
	problems:
	1. what is the relationship between print information and car's verb?
		sementically they are two different things.
		so I should not print something in car' method.

	2. what we want is always: print the map.
		what is in the map we printed?
			(1) current location
			(2) goal
			(3) the obstacle of we detected.
			(4) the path we planned this time.

		what is the "the status of the car"?
			I think
			(1) current location of the car.
			(2) goal location of the car.
			(3) the obstacle of we detected.
			are "the status of the car"

		what is not "the status of the car"?
			I think
			(1) the the path we planned this time.
			is not "the status of the car".
*/

#ifndef CAR_ENTITY
#define CAR_ENTITY

#include <vector>
#include <iostream>
#include <cmath>
#include "utility.h"
#include <memory>
#include <cmath>
#include <climits>

struct map_node
{
public:
	//the raw notation of the map.example: S, G, X...
	char notation;
	int heuristic_value;
	int key_value;
	//point to the previous point on its shortest route to the start point.
	int parent_x;
	int parent_y;
public:
	//default no barrier in the map, every map_node is blank.
	//prev_i prev_j is set to INT_MAX, which means that the shortest path to the start point is undefined.
	map_node()
	{
		notation = char(95);
		heuristic_value = 0;
		key_value = 0;
		parent_x = INT_MAX;
		parent_y = INT_MAX;
	}
public:
	//because map_node is the element of the matrix.
	//for the convinence of printing the matrix, we add a friend function here.
	friend std::ostream&
	operator<<(std::ostream& os, const map_node& node)
	{
		os << node.notation;
		return os;
	}
};


class car
{
public:
	static constexpr int dir_cnt = 4;
	static constexpr int dirs[dir_cnt][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
protected:
	//position
	int cur_pos_x;
	int cur_pos_y;

	//goal
	int goal_x;
	int goal_y;

	//raw map
	std::vector<std::vector<char>> real_map;
	//car view map stores in the matrix
	//and the current shortest route also stores here.
	std::vector<std::vector<map_node>> carview_map;

	int planning_cnt;
	int total_node_expanded_accumulator;
protected:
	//update the carview_map if there are some barrier in its surrending area.
	void
	detect();

	int
	get_manhattan_distance(int x1, int y1, int x2, int y2)
	{
		return std::abs(x1 - x2) + std::abs(y1 - y2);
	}

	virtual void
	set_heuristic_value(int x, int y) = 0;

	void
	write_parent_N_kvalue(int father_x, int father_y, int son_x, int son_y)
	{
		//set k-value
		carview_map[son_x][son_y].key_value = carview_map[father_x][father_y].key_value + 1;
		//set parent
		carview_map[son_x][son_y].parent_x = father_x;
		carview_map[son_x][son_y].parent_y = father_y;
	}

	void
	update_parent_N_kvalue(int father_x, int father_y, int son_x, int son_y)
	{
		if (carview_map[father_x][father_y].key_value + 1 < carview_map[son_x][son_y].key_value)
		{
			write_parent_N_kvalue(father_x, father_y, son_x, son_y);
		}
	}

	/*
		accoding to the current car view map.
		we can search and plan a shortest path.

		return value is can we reach the goal.
		expand_node_cnt is how many node are expanded.
	*/
	virtual bool
	planning(int* total_node_expanded_accumulator) = 0;

	/*
		simply doing the a star search.
		after the search, we get
		0. number of nodes we expand.
		1. the path is exist or not?(the return value)
		2. how long is the current start point to the goal?(*distance_to_goal)
		3. what point is in the closed list at the end?(closed_list_owner_indirect)
		4. what point is in the open list at the end?(open_list_owner_indirect)
	*/
	bool
	planning_Astar(int* total_node_expanded_accumulator,
	               int* distance_to_goal = nullptr,
	               std::unique_ptr<std::vector<std::pair<int, int>>>* closed_list_owner_indirect = nullptr,
	               std::unique_ptr<std::vector<std::pair<int, int>>>* open_list_owner_indirect = nullptr);

	bool
	can_move_along(std::pair<int, int> next_point)
	{
		return carview_map[next_point.first][next_point.second].notation != 'x';
	}

	void
	move_one_step(std::pair<int, int> next_point)
	{
		cur_pos_x = next_point.first;
		cur_pos_y = next_point.second;
	}

	void print_map_header();

	void print_map_with_path(std::unordered_set<std::pair<int, int>, functor_util::myhash_forpair>* path_list_ptr);

	void print_failed_status();

	void
	showpath_N_move_along();

public:
	car() = delete;
	car(const std::string& map_file_fullname);

	file_util::search_info
	going_N_showpath();

	void
	print_carviewmap()
	{
		debug_util::print_matrix(this->carview_map);
	}

	void
	print_initialmap()
	{
		std::cout << "print initial realmap" << std::endl;
		std::cout << "row: " << this->real_map.size() << std::endl;
		std::cout << "column: " << this->real_map[0].size() << std::endl;
		debug_util::print_matrix(this->real_map);
		std::cout << "--realmap print end-- \n" << std::endl;
	}

	void print_search_info()
	{
		std::cout << "Print search info:\n";
		std::cout << "Total search: " << planning_cnt << " times.\n";
		std::cout << "Total_expand_nodes:" << total_node_expanded_accumulator << std::endl;
	}

	virtual ~car() = default;
};

class car_Astar: public car
{
public:
	car_Astar(const std::string& map_file_fullname);
protected:
	virtual void
	set_heuristic_value(int x, int y) override
	{
		carview_map[x][y].heuristic_value = get_manhattan_distance(x, y, goal_x, goal_y);
	}

	virtual bool
	planning(int* total_node_expanded_accumulator) override;
};


class car_Adaptive_Astar: public car
{
public:
	car_Adaptive_Astar(const std::string& map_file_fullname);
protected:
	virtual void
	set_heuristic_value(int x, int y) override
	{
		carview_map[x][y].heuristic_value =
		    carview_map[x][y].heuristic_value == INT_MAX ?
		    get_manhattan_distance(x, y, goal_x, goal_y) : std::max(carview_map[x][y].heuristic_value, get_manhattan_distance(x, y, goal_x, goal_y));
	}
	virtual bool
	planning(int* total_node_expanded_accumulator) override;
};


#endif