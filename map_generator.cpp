#include <iostream>
#include <vector>
#include <memory>
#include <utility>
#include <queue>
#include <ctime>
#include <fstream>

constexpr int dirs[4][2] = {{0,  1}, {0,  -1}, {1,  0}, { -1, 0}};

static const auto prepare_func = []()
{
    std::srand(time(0));
    std::cout << "run srand(time(0)) at the beginning." << std::endl;
    return nullptr;
}();



class Graph
{
public:
    int height;
    int width;
    std::vector<std::vector<char>> map_matrix;
    //'_' -> land
    //'X' -> sea

private:
    typedef std::pair<int, int> node_coor;
    node_coor get_random_node_in_area(int area_height, int area_width)
    {
        int x = random() % area_height;
        int y = random() % area_width;
        return std::make_pair(x, y);
    }

    void set_node_value(node_coor coor, const char notation)
    {
        map_matrix[coor.first][coor.second] = notation;
    }

    /*
            A(left,top point)
            *
                                       C(new point)
                  B(right,down point)
                  *
    */
    bool is_in_room(node_coor lefttop_coor, node_coor rightdown_coor, node_coor new_point_coor)
    {
        int A_x = lefttop_coor.first;
        int A_y = lefttop_coor.second;
        int B_x = rightdown_coor.first;
        int B_y = rightdown_coor.second;
        int C_x = new_point_coor.first;
        int C_y = new_point_coor.second;
        return (A_x - C_x) * (B_x - C_x) + (A_y - C_y) * (B_y - C_y) < 0;
    }

    bool is_in_sea(node_coor coor)
    {
        return map_matrix[coor.first][coor.second] == 'x';
    }

    bool is_in_bound(node_coor coor)
    {

    }

    node_coor add_vector(node_coor coor, int x, int y)
    {
        int new_x = coor.first + x;
        int new_y = coor.second + y;
        return std::make_pair(new_x, new_y);
    }

public:
    void generate_random_sea(int radius, int sea_num, double prob)
    {
        for (int generated_sea_num = 0; generated_sea_num < sea_num; ++generated_sea_num)
        {

            //bfs to generate sea
            std::queue<node_coor> open_nodes;
            open_nodes.push(get_random_node_in_area(height, width));

            for (int d = 0; d < radius; ++d)
            {
                int cursize = open_nodes.size();
                for (int cnt = 0; cnt < cursize; ++cnt)
                {
                    //expand
                    auto curnode = open_nodes.front();
                    open_nodes.pop();

                    set_node_value(curnode, 'x');
                    //generate
                    for (int dir = 0; dir < 4; ++dir)
                    {
                        if (double(rand()) / RAND_MAX < prob)
                        {
                            auto new_node = add_vector(curnode, dirs[dir][0], dirs[dir][1]);
                            if (!is_in_bound(new_node) || !is_in_sea(new_node)) continue;
                            open_nodes.push(new_node);
                        }
                    }
                }
            }
        }
    }

public:
    void generate_random_rooms(int room_cnt, int wall_len)
    {
        std::vector<pair<node_coor, node_coor>> rooms;
        for (int i = 0; i < room_cnt; ++i)
        {
            node_coor coor_A;
            node_coor coor_B;
            
        }
    }

    void select_random_start_end()
    {
        node_coor start_coor;
        do
        {
            start_coor = get_random_node_in_area(height, width);
        } while (is_in_sea(start_coor));
        set_node_value(start_coor, 's');

        node_coor goal_coor;
        do
        {
            goal_coor = get_random_node_in_area(height, width);
        } while (is_in_sea(goal_coor));
        set_node_value(goal_coor, 'g');
    }


public:
    Graph() = default;

    Graph(int width, int height)
    {
        //initialize->all lands
        this->width = width;
        this->height = height;
        map_matrix = std::vector<std::vector<char>>(height, std::vector<char>(width, '_'));
    }

    void show()
    {
        for (int i = 0; i < height; ++i)
        {
            for (int j = 0; j < width; ++j)
            {
                std::cout << map_matrix[i][j] << " ";
            }
            std::cout << '\n';
        }
        std::cout << std::flush;
    }

    void output(const std::string & filename)
    {
        std::fstream fout;
        fout.open(filename, std::ios_base::out);
        if (fout.is_open())
        {
            fout << map_matrix.size() << "\n";
            fout << map_matrix[0].size() << "\n";
            for (int i = 0; i < height; ++i)
            {
                for (int j = 0; j < width; ++j)
                {
                    fout << map_matrix[i][j] << " ";
                }
                fout << '\n';
            }
            fout << std::flush;
        }
    }
};


int main(int argc, char* argv[])
{
    Graph graph(20, 20);
    graph.generate_random_sea(4, 20, 0.8);
    graph.select_random_start_end();
    graph.show();
    graph.output(argv[1]);
    return 0;
}