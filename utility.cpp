#include "utility.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cctype>

std::vector<std::vector<char>> file_util::read_map(const std::string& filename, int& start_x, int& start_y, int& goal_x, int& goal_y)
{

	int read_bufsize = 0;

#ifdef READ_BUFSIZE
	read_bufsize = READ_BUFSIZE;
#else
	read_bufsize = 4096;
#endif

	std::ifstream fin;
	fin.open(filename, std::ios_base::in);
	if (fin.is_open())
	{
		char* buf = new char[read_bufsize];
		fin.getline(buf, read_bufsize);

		if (!std::isdigit(buf[0]))
		{
			std::cerr << "your input may not satisfy our format.\n";
			std::cerr << "format:\nNumber of rows in the gridworld\nNumber of columns in the gridworld\nGridworld\n\nLegend:\ns := start cell\ng := goal cell\nx := blocked cell (obstacle)\n:= unblocked cell\n";
		}
		int row = stoi(std::string(buf));
		fin.getline(buf, read_bufsize);
		int col = stoi(std::string(buf));

#ifdef DEBUG
		std::cout << row << std::endl;
		std::cout << col << std::endl;
#endif
		std::vector<std::vector<char>> map_matrix;
		while (fin.getline(buf, read_bufsize))
		{
			map_matrix.push_back(std::vector<char>());
			char* ptr_copy = buf;
			while (*ptr_copy != '\0')
			{
#ifdef DEBUG
				std::cout << *ptr_copy << std::endl;
				std::cout << short(*ptr_copy) << std::endl;
#endif
				//std::cout << "char:" << *ptr_copy << std::endl;
				//std::cout << "short" << short(*ptr_copy) << std::endl;

				switch (*ptr_copy)
				{
				case 's':
				{
					start_x = map_matrix.size() - 1;
					start_y = map_matrix.back().size();
					map_matrix.back().push_back(*ptr_copy);
					break;
				}
				case 'g':
				{
					goal_x = map_matrix.size() - 1;
					goal_y = map_matrix.back().size();
					map_matrix.back().push_back(*ptr_copy);
					break;
				}
				case '_':
				{
					map_matrix.back().push_back(*ptr_copy);
					break;
				}
				case 'x':
				{
					map_matrix.back().push_back(*ptr_copy);
					break;
				}
				default: break;
				}
				++ptr_copy;
			}
			//std::cout << short(*ptr_copy) << std::endl;
		}

		if (map_matrix.back().size() == 0) map_matrix.pop_back();
#ifdef DEBUG
		debug_util::print_matrix(map_matrix);
#endif
		fin.close();
		delete[] buf;
		return map_matrix;
	}
	else
	{
		std::cerr << "open file " << "'" << filename << "'" << "failed\n";
		exit(1);
		return {};
	}
}
