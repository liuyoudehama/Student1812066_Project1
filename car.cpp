#include "car.h"
#include "utility.h"
#include <vector>
#include <functional>
#include <queue>
#include <cassert>
#include <stack>
#include <unordered_set>
#include <set>

constexpr int car::dir_cnt;
constexpr int car::dirs[dir_cnt][2];//why....?why....? always linking error without this....

void
car::detect()
{
	for (int dir = 0; dir < dir_cnt; ++dir)
	{
		int sur_x = cur_pos_x + dirs[dir][0];
		int sur_y = cur_pos_y + dirs[dir][1];

		if (sur_x >= 0 && sur_x < carview_map.size() && sur_y >= 0 && sur_y < carview_map[0].size() && real_map[sur_x][sur_y] == 'x')
		{
			carview_map[sur_x][sur_y].notation = 'x';
		}
	}
}

bool
car::planning_Astar(int* total_node_expanded_accumulator,
                    int* distance_to_goal,
                    std::unique_ptr<std::vector<std::pair<int, int>>>* closed_list_owner_indirect,
                    std::unique_ptr<std::vector<std::pair<int, int>>>* open_list_owner_indirect)
{
	//1.use a openlist to keep up with open list. use tree set.
	//because we need:
	//1-find the most deserved expand node in the list. 2-find a certain node in the list or not.
	auto mycomp =
	    [this](const std::pair<int, int>& a, const std::pair<int, int>& b)
	{
		int ax = a.first;
		int ay = a.second;
		int bx = b.first;
		int by = b.second;
		if (this->carview_map[ax][ay].key_value + this->carview_map[ax][ay].heuristic_value == this->carview_map[bx][by].key_value + this->carview_map[bx][by].heuristic_value)
		{
			if (this->carview_map[ax][ay].key_value == this->carview_map[bx][by].key_value)
			{	if (ax == bx) return ay < by;
				else return ax < bx;
			}
			else return this->carview_map[ax][ay].heuristic_value < this->carview_map[bx][by].heuristic_value;
		}
		else
		{
			return this->carview_map[ax][ay].key_value + this->carview_map[ax][ay].heuristic_value < this->carview_map[bx][by].key_value + this->carview_map[bx][by].heuristic_value;
		}
	};

	std::set<std::pair<int, int>, decltype(mycomp)> openlist_set(mycomp);
	//2.put the start point to the open list.

	//!!!override this.
	set_heuristic_value(cur_pos_x, cur_pos_y);//vaildate the node
	openlist_set.insert(std::make_pair(cur_pos_x, cur_pos_y));

	//3.also need a closedlist.
	std::unordered_set<std::pair<int, int>, functor_util::myhash_forpair> closedlist_set;

	bool search_status = false; //denotes we find a path to the goal or not.

	std::cout << std::endl;
	while (!openlist_set.empty())
	{
		auto trav_cur_expand = *openlist_set.begin();
		openlist_set.erase(openlist_set.begin());

		closedlist_set.insert(trav_cur_expand);

		int trav_cur_expand_x = trav_cur_expand.first;
		int trav_cur_expand_y = trav_cur_expand.second;
		//when the goal is in the closed list. travsering finished.
		if (trav_cur_expand_x == goal_x && trav_cur_expand_y == goal_y)
		{
			search_status = true;
			if (distance_to_goal)
			{
				*distance_to_goal = carview_map[trav_cur_expand_x][trav_cur_expand_y].key_value;
			}
			break;
		}

		//try to generate all neighbors.
		for (int i = 0; i < dir_cnt; ++i)
		{
			int trav_gen_x = trav_cur_expand_x + dirs[i][0];
			int trav_gen_y = trav_cur_expand_y + dirs[i][1];

			if (trav_gen_x < 0 || trav_gen_x >= carview_map.size() || trav_gen_y < 0 || trav_gen_y >= carview_map[0].size()
			        || carview_map[trav_gen_x][trav_gen_y].notation == 'x'
			        || closedlist_set.find(std::make_pair(trav_gen_x, trav_gen_y)) != closedlist_set.end())
			{
				continue;
			}
			else if (openlist_set.find(std::make_pair(trav_gen_x, trav_gen_y)) != openlist_set.end())
			{
				openlist_set.erase(openlist_set.find(std::make_pair(trav_gen_x, trav_gen_y)));
				update_parent_N_kvalue(trav_cur_expand_x, trav_cur_expand_y, trav_gen_x, trav_gen_y);
				openlist_set.insert(std::make_pair(trav_gen_x, trav_gen_y));
			}
			else
			{
				//calculate the h-value.
				set_heuristic_value(trav_gen_x, trav_gen_y);
				write_parent_N_kvalue(trav_cur_expand_x, trav_cur_expand_y, trav_gen_x, trav_gen_y);
				//the point is ready to be put in open list.
				openlist_set.insert(std::make_pair(trav_gen_x, trav_gen_y));
			}
		}
	}

	//prepare for the by-products for the search.
	*total_node_expanded_accumulator += closedlist_set.size();

	if (closed_list_owner_indirect)
	{
		std::unique_ptr<std::vector<std::pair<int, int>>> closedlist_vec_owner(new std::vector<std::pair<int, int>>(closedlist_set.size()));
		for (auto node : closedlist_set)
		{
			closedlist_vec_owner->push_back(node);
		}
		*closed_list_owner_indirect = std::move(closedlist_vec_owner);
	}
	if (open_list_owner_indirect)
	{
		std::unique_ptr<std::vector<std::pair<int, int>>> openlist_vec_owner(new std::vector<std::pair<int, int>>(openlist_set.size()));
		for (auto node : openlist_set)
		{
			openlist_vec_owner->push_back(node);
		}
		*open_list_owner_indirect = std::move(openlist_vec_owner);
	}

	return search_status;
}

void
car::print_map_header()
{
	//header info part1(optional)
	if (planning_cnt > 1) std::cout << "found a obstacle en-route, start replanning\n";
	//header info part2
	std::cout << "# of plan: " << planning_cnt;
	//header info part3
	if (planning_cnt == 1) std::cout << "(initial)" << std::endl;
	else std::cout << "(replan)" << std::endl;
}

void
car::print_map_with_path(std::unordered_set<std::pair<int, int>, functor_util::myhash_forpair>* path_list_ptr)
{
	print_map_header();
	if (this->carview_map.size() == 0 || this->carview_map[0].size() == 0) return;

	std::cout << "  ";

	for (int col_index = 0; col_index < this->carview_map[0].size(); ++col_index)
	{
		std::cout << col_index;
		if (col_index < 10) std::cout << " ";
	}

	std::cout << "\n";

	for (int row_index = 0; row_index < this->carview_map.size(); ++row_index)
	{
		std::cout << row_index;
		if (row_index < 10) std::cout << " ";

		for (int col_index = 0; col_index < this->carview_map[0].size(); ++col_index)
		{
			//start point and other point are not in path list
			//goal point and path point are in path list
			if (!path_list_ptr || path_list_ptr->find(std::make_pair(row_index, col_index)) == path_list_ptr->end())
			{
				if (row_index == cur_pos_x && col_index == cur_pos_y) std::cout << "s ";
				else std::cout << this->carview_map[row_index][col_index] << " ";
			}
			else
			{
				if (row_index == goal_x && col_index == goal_y) std::cout << "g ";
				else std::cout << "o ";
			}
		}
		std::cout << "\n";
	}
	std::cout << std::flush;
}

void
car::print_failed_status()
{
	print_map_header();
	if (this->carview_map.size() == 0 || this->carview_map[0].size() == 0) return;

	std::cout << "  ";

	for (int col_index = 0; col_index < this->carview_map[0].size(); ++col_index)
	{
		std::cout << col_index;
		if (col_index < 10) std::cout << " ";
	}

	std::cout << "\n";

	for (int row_index = 0; row_index < this->carview_map.size(); ++row_index)
	{
		std::cout << row_index;
		if (row_index < 10) std::cout << " ";

		for (int col_index = 0; col_index < this->carview_map[0].size(); ++col_index)
		{
			if (row_index == cur_pos_x && col_index == cur_pos_y)
			{
				std::cout << "s ";
			}
			else if (row_index == goal_x && col_index == goal_y)
			{
				std::cout << "g ";
			}
			else
			{
				std::cout << this->carview_map[row_index][col_index] << " ";
			}
		}
		std::cout << "\n";
	}
	std::cout << std::flush;
	std::cout << "No route found" << std::endl;
}

void
car::showpath_N_move_along()
{
	//all node from the current location to the goal will be in it from top to bottom.
	std::stack<std::pair<int, int>> path;
	int cur_path_node_x = goal_x;
	int cur_path_node_y = goal_y;
	std::unique_ptr<std::unordered_set<std::pair<int, int>, functor_util::myhash_forpair>> path_list_ptr(new std::unordered_set<std::pair<int, int>, functor_util::myhash_forpair>);

	//collect path nodes in stack and set.
	while (cur_path_node_x != cur_pos_x || cur_path_node_y != cur_pos_y)
	{
		path.push(std::make_pair(cur_path_node_x, cur_path_node_y));
		path_list_ptr->insert(std::make_pair(cur_path_node_x, cur_path_node_y));
		int next_path_node_x = this->carview_map[cur_path_node_x][cur_path_node_y].parent_x;
		int next_path_node_y = this->carview_map[cur_path_node_x][cur_path_node_y].parent_y;
		cur_path_node_x = next_path_node_x;
		cur_path_node_y = next_path_node_y;
	}

	//show path
	car::print_map_with_path(path_list_ptr.get());

	//move along
	while (!path.empty() && car::can_move_along(path.top()))
	{
		car::move_one_step(path.top());
		path.pop();	//update path
		car::detect();
	}
}

car::car(const std::string& map_file_fullname)
{
	//read the real map, set the current location and goal location of the car.
	this->real_map = file_util::read_map(map_file_fullname, cur_pos_x, cur_pos_y, goal_x, goal_y);

	//check vaildation of map matrix.
	//another asumption is that every row has same size.
	assert(this->real_map.size() > 0);
	assert(this->real_map[0].size() > 0);

	//build a carview map
	this->carview_map = std::vector<std::vector<map_node>>(this->real_map.size(), std::vector<map_node>(this->real_map[0].size()));
	this->planning_cnt = 0;
	this->total_node_expanded_accumulator = 0;
}

file_util::search_info
car::going_N_showpath()
{
	car::detect();

	while (cur_pos_x != goal_x || cur_pos_y != goal_y)
	{
		++planning_cnt;
		if (!this->planning(&total_node_expanded_accumulator)) break;
		car::showpath_N_move_along();
	}

	if (cur_pos_x != goal_x || cur_pos_y != goal_y)
	{
		car::print_failed_status();
	}

	return file_util::search_info(planning_cnt, total_node_expanded_accumulator);
}

car_Astar::car_Astar(const std::string& map_file_fullname): car(map_file_fullname) {}

bool
car_Astar::planning(int* total_node_expanded_accumulator)
{
	int distance_to_goal = 0;
	std::unique_ptr<std::vector<std::pair<int, int>>> closedlist_owner(nullptr);
	std::unique_ptr<std::vector<std::pair<int, int>>> openlist_owner(nullptr);
	bool planning_status = car::planning_Astar(total_node_expanded_accumulator, &distance_to_goal, &closedlist_owner, &openlist_owner);

	return planning_status;
}

car_Adaptive_Astar::car_Adaptive_Astar(const std::string& map_file_fullname): car(map_file_fullname) {}

bool
car_Adaptive_Astar::planning(int* total_node_expanded_accumulator)
{
	int distance_to_goal = 0;
	std::unique_ptr<std::vector<std::pair<int, int>>> closedlist_owner(nullptr);
	std::unique_ptr<std::vector<std::pair<int, int>>> openlist_owner(nullptr);
	bool planning_status = car::planning_Astar(total_node_expanded_accumulator, &distance_to_goal, &closedlist_owner, &openlist_owner);

	for (auto& node : * (openlist_owner.get()))
	{
		this->carview_map[node.first][node.second].heuristic_value = INT_MAX;
	}

	for (auto& node : * (closedlist_owner.get()))
	{
		this->carview_map[node.first][node.second].heuristic_value = distance_to_goal - this->carview_map[node.first][node.second].key_value;
	}

	return planning_status;
}
