CC = g++
STDFLAG = -std=c++11
UTIL_SRC = utility.cpp
UTIL_LIB = utility.o
CAR_SRC = car.cpp
CAR_LIB = car.o
EXECUTABLE = Astar

current_dir = $(shell pwd)
PATH:=${PATH}:$(current_dir)
	
build: car.o uiility.o
	$(CC) main.cpp $(STDFLAG) $(UTIL_LIB) $(CAR_LIB) -o $(EXECUTABLE)
	@echo "usage: ./Astar -i inputfile [-A]"

profile: build
	$(CC) -c $(STDFLAG) $(CAR_SRC) -o $(CAR_LIB) -g
	$(CC) -c $(STDFLAG) $(UTIL_SRC) -o $(UTIL_LIB) -g
	$(CC) main.cpp  -g $(STDFLAG) $(UTIL_LIB) $(CAR_LIB) -o $(EXECUTABLE)
	@echo "valgrind --tool=callgrind ./Astar -i inputfile [-A]"

reportinfo: car.o uiility.o
	$(CC) report.cpp $(STDFLAG) $(UTIL_LIB) $(CAR_LIB) -o report
	@echo "usage: ./report"
	
generatemap: map_generator.cpp
	$(CC) map_generator.cpp $(STDFLAG) -o generatemap
	@echo "usage: ./generatemap outputfile"
	
car.o: car.h $(CAR_SRC)
	$(CC) -c $(STDFLAG) $(CAR_SRC) -o $(CAR_LIB)

uiility.o: utility.h $(UTIL_SRC)
	$(CC) -c $(STDFLAG) $(UTIL_SRC) -o $(UTIL_LIB)

.PHONY : clean

clean:
	rm *.o
	rm $(EXECUTABLE)